﻿namespace SellCore.Entity.Models
{
    public class ConfigSystem : Agent
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Value { get; set; }
    }
}
