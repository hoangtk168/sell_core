﻿using Microsoft.EntityFrameworkCore;
using SellCore.Entity.Models;
using System.Configuration.Internal;
using Microsoft.Extensions.Configuration;

namespace SellCore.Entity
{
    public class Context : DbContext
    {
        public IConfiguration Configuration { get; }
        public Context(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        DbSet<ConfigSystem> ConfigSystem { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var connectionString = Configuration.GetConnectionString("MySqlConnection");
            optionsBuilder.UseSqlServer(connectionString);
        }
    }
}
